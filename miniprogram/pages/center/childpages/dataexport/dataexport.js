const util = require('../../../../utils/util.js');
const keyService = require('../../../../data/keyService.js');
const globalData = getApp().globalData;

Page({
  data: {
    exportType: 1,
    dataText: '',
    saveText: '导入'
  },
  //格式化导入时的输入框
  exportBind: function(event) {
    const inputValue = event.detail.value;
    this.data.dataText = inputValue;
  },
  okBind: function(event) {
    const exportType = this.data.exportType;
    if (exportType == 1 || exportType == 2) {
      util.setClipboardData(this.data.dataText);
      return;
    }
    //导入操作；未完成


    util.showToast('导入成功！');
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const exportType = options.exportType;

    if (exportType == 3) { //格式化导入
      this.setData({
        exportType: exportType
      })
      return;
    }

    let dataText = '';
    let saveText = '复制到剪切板';
    //格式化导出
    let localJsonData = util.getStorageData(globalData.localJsonName);
    if (localJsonData == '') {
      this.setData({
        exportType: exportType,
        dataText: dataText,
        saveText: saveText
      })
      return;
    }
    localJsonData = keyService.deJsonData(localJsonData, globalData.password);
    if (exportType == 1) {
      for (let i = 0; i < localJsonData.key.length; i++) {
        const keyModel = localJsonData.key[i];

        dataText += '钥匙名称：' + keyModel.keyName;
        if (keyModel.keyAccount)
          dataText += '\n账号：' + keyModel.keyAccount;
        if (keyModel.keyPwd)
          dataText += '\n密码：' + keyModel.keyPwd;
        if (keyModel.keyPwd2)
          dataText += '\n密码2：' + keyModel.keyPwd2;
        if (keyModel.keyUrl)
          dataText += '\nURL：' + keyModel.keyUrl;
        if (keyModel.keyRemark)
          dataText += '\n备注：' + keyModel.keyRemark;
        dataText += '\n\n';
      }
    } else if (exportType == 2) {
      const temp = {};
      temp.version = localJsonData.version;
      temp.time = localJsonData.time;
      temp.category = localJsonData.category;
      temp.key = localJsonData.key;

      dataText = JSON.stringify(temp);
    }

    this.setData({
      exportType: exportType,
      dataText: dataText,
      saveText: saveText
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  }
})
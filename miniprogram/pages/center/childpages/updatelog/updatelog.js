Page({
  data: {
    versionArry: []
  },
  onLoad: function(options) {
    const versionArry = [];
    versionArry.push({
      time: '2020-09-1',
      version: 'V2.0.1',
      title: '常规升级',
      contents: [
        '[升级]升级基础库，优化输入密码页。',
        '[适配]适配PC端小程序'
      ]
    });
    versionArry.push({
      time: '2019-09-2',
      version: 'V2.0.0',
      title: '小程序正式命名为SKey',
      contents: [
        '[优化]常见问题中，增加对SKey名称由来的说明。'
      ]
    });
    versionArry.push({
      time: '2019-08-30',
      version: 'V1.5.0',
      title: '正式版',
      contents: [
        '[新增]使用指南页、常见问题页、更新日志页。',
        '[优化]小程序首次使用、密码验证、同步数据等功能的页面；并调整跳转逻辑更加清晰。',
        '[修复]清除数据、格式化数据功能验证主密码，防止误删除',
      ]
    });
    versionArry.push({
      time: '2019-08-20',
      version: 'V1.0.0',
      title: '正式版发布',
      contents: [
        '[完成]指纹功能。',
        '[完成]数据同步功能。',
        '[修复]大量测试并修复各页面问题。',
      ]
    });
    versionArry.push({
      time: '2019-08-15',
      version: 'V0.4.0',
      title: '开发版，内测',
      contents: [
        '[完成]完成数据存储结构修改，并完成相关页面重构工作。',
        '[完成]设置中心自定义分类、修改密码、使用指南、关于等页面。',
        '[完成]CryptoJS加解密库的移植、适配小程序等。',
        '[完成]密码验证页、数据加解密处理等。'
      ]
    });
    versionArry.push({
      time: '2019-08-10',
      version: 'V0.3.0',
      title: '开发版，内测',
      contents: [
        '[完成]首页自定义状态栏，并适配吸顶效果。',
        '[完成]设置中心自定义分类、修改密码、使用指南、关于等页面。',
        '[完成]CryptoJS加解密库的移植、适配小程序等。',
        '[完成]密码验证页、数据加解密处理等。'
      ]
    });
    versionArry.push({
      time: '2019-08-5',
      version: 'V0.2.0',
      title: '初始开发版上线，开始内测',
      contents: [
        '[完成]首页功能，并实现吸顶效果。',
        '[完成]账号密码查看页。',
        '[完成]账号密码编辑页，颜色随分类变化。',
        '[完成]账号密码的增删改功能。',
        '[完成]个人设置中心页布局等。'
      ]
    });
    versionArry.push({
      time: '2019-08-01',
      version: 'V0.1.0',
      title: '正式立项',
      contents: []
    });

    this.setData({
      versionArry: versionArry
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  }
})
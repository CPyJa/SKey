const log = require('./utils/log.js');
const nowTime = new Date().getTime();
App({
  globalData: {
    navBarInfo: {
      system: '',
      statusHeight: 0, //状态栏高度
      navHeight: 0, //导航栏高度
      titleBarHeight: 0, //胶囊高度
      titleBarTop: 0, //胶囊离状态栏高度
    },
    userInfo: {
      openID: "",
      unionID: "",
      enPassword: "",
      isCloudSyn: false,
      nickName: "",
      avatarUrl: "",
      gender: "",
      province: "",
      city: "",
      country: "",
      language: "",
      time: nowTime
    },
    cloudJsonName: "",
    localJsonName: "localJson",
    localConfigName: "localConfig",
    isLocalFirstUse: true,
    isLogin: false,
    hasInfo: false,
    password: "",
    authenPassword: "",
    time: nowTime
  },
  onLaunch: function() {
    this.initCloud();
    this.initLocalConfig();
    this.initNavBarInfo();
  },
  onError: function(err) {
    log.error(err);
  },
  //初始化云开发
  initCloud() {
    if (wx.cloud) {
      wx.cloud.init({
        //env: 'sinovel', //'dev-SKey',
        traceUser: true,
      })
    }
  },
  //加载本地配置
  initLocalConfig() {
    const localConfig = wx.getStorageSync(this.globalData.localConfigName);
    if (localConfig != '') {
      this.globalData = localConfig;
      this.globalData.password = '';
    } else {
      wx.setStorageSync(this.globalData.localConfigName, this.globalData);
    }
  },
  //初始化屏幕信息
  initNavBarInfo() {
    const globalData = this.globalData;
    if (globalData.navBarInfo.system != '') //本地已加载过
      return;

    const sysInfo = wx.getSystemInfoSync(); //状态栏信息
    const titleBarInfo = wx.getMenuButtonBoundingClientRect(); //胶囊信息

    const statusHeight = sysInfo.statusBarHeight; //状态栏高度
    const titleBarHeight = titleBarInfo.height; //胶囊高度

    const navBarInfo = globalData.navBarInfo;
    let titleTop = titleBarInfo.top - sysInfo.statusBarHeight; //胶囊离状态栏高度
    if (titleTop <= 6) //例如特别小的手机 iphone 7 默认最小间距是6
      titleTop = 6;
    navBarInfo.system = sysInfo.system;
    navBarInfo.statusHeight = statusHeight;
    navBarInfo.navHeight = titleBarHeight + titleTop * 2;
    navBarInfo.titleBarHeight = titleBarHeight;
    navBarInfo.titleBarTop = titleTop;

    let localConfig = wx.getStorageSync(globalData.localConfigName);
    localConfig.navBarInfo = navBarInfo;

    wx.setStorageSync(globalData.localConfigName, localConfig);
  }
})